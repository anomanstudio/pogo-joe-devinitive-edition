using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdjustmentManager : MonoBehaviour
{
    public int pogoSpeed, monsterSpeed, pogoLimitSpeed, monsterLimitSpeed;
    public Text pogoSpeedTxt, monsterSpeedTxt;
    // Start is called before the first frame update
    void Start()
    {
        // pogoSpeedTxt = GetComponent<Text>();
        // monsterSpeedTxt = GetComponent<Text>();
        pogoSpeedTxt.text = pogoSpeed.ToString();
        monsterSpeedTxt.text = monsterSpeed.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        pogoSpeedTxt.text = pogoSpeed.ToString();
        monsterSpeedTxt.text = monsterSpeed.ToString();
    }

    public void addPogoSpeed(){
        if(pogoSpeed >= 0 && pogoSpeed < pogoLimitSpeed){
            pogoSpeed++;
        }
        else
        {
            pogoSpeed = pogoLimitSpeed;
        }
    }
    public void addMonsterSpeed(){
        if(monsterSpeed >= 0 && monsterSpeed < monsterLimitSpeed){
            monsterSpeed++;
        }
        else
        {
            monsterSpeed = monsterLimitSpeed;
        }
    }
    public void substractPogoSpeed(){
        if(pogoSpeed > 0){
            pogoSpeed--;
        }
        else
        {
            pogoSpeed = 0;
        }
    }
    public void substractMonsterSpeed(){
        if(monsterSpeed > 0){
            monsterSpeed--;
        }
        else
        {
            monsterSpeed = 0;
        }
    }
    
}
