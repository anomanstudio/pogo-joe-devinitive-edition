using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Player_Controller : MonoBehaviour
{
    public Tiles_Behaviour m_currentTile;
    public float m_moveSpeed = 1;

    public SpriteRenderer m_playerSprite;

    public bool m_enableJump;
    public bool m_alreadyMove;
    public bool m_afterTeleport;

    public Animator m_animator;

    private Coroutine m_Coroutine_teleport;

    // Start is called before the first frame update
    void Start()
    {
        m_currentTile = Tiles_Controller.instance.m_playerStartPosition;
        transform.position = m_currentTile.transform.position;
        m_animator = GetComponent<Animator>();
        m_currentTile.f_SubstractSteps();
        Tiles_Controller.instance.m_player = this;
    }

    // Update is called once per frame
    void Update()
    {
        if (!m_alreadyMove)
        {
            if (Input.GetButtonDown("Down") && m_enableJump)
            {
                if (m_currentTile.m_tilesDown != null)
                {
                    f_SetCurrentTile(m_currentTile.m_tilesDown, false);
                    f_RandomFlip();
                }
            }


            if (Input.GetButtonDown("Up") && m_enableJump)
            {
                if (m_currentTile.m_tilesUp != null)
                {
                    f_SetCurrentTile(m_currentTile.m_tilesUp, false);
                    f_RandomFlip();
                }
            }


            if (Input.GetButtonDown("Left") && m_enableJump)
            {
                if (m_currentTile.m_tilesLeft != null)
                {
                    f_SetCurrentTile(m_currentTile.m_tilesLeft, false);
                    m_playerSprite.flipX = true;
                }
            }


            if (Input.GetButtonDown("Right") && m_enableJump)
            {
                if (m_currentTile.m_tilesRight != null)
                {
                    f_SetCurrentTile(m_currentTile.m_tilesRight, false);
                    m_playerSprite.flipX = false;
                }
            }
        }
    }

    public void f_SetCurrentTile(Tiles_Behaviour p_tile, bool isTeleport)
    {
        m_currentTile = p_tile;

        if (!isTeleport)
        {
            StartCoroutine(ie_JumpingSmoothTiles(m_moveSpeed, p_tile.transform.localPosition));
            //StartCoroutine(ie_SmoothLerp(m_moveSpeed, transform.localPosition, p_tile.transform.localPosition));
        }
        else
        {
            if (m_Coroutine_teleport != null)
            {
                StopCoroutine(m_Coroutine_teleport);
                m_Coroutine_teleport = null;
            }
            m_Coroutine_teleport = StartCoroutine(ie_JumpingTeleportTiles(p_tile.transform.localPosition, false, m_moveSpeed));
        }
    }

    public void f_Respawn()
    {
        transform.DOComplete();
        StopAllCoroutines();

        if (m_Coroutine_teleport != null)
        {
            StopCoroutine(m_Coroutine_teleport);
            m_Coroutine_teleport = null;
        }
        m_Coroutine_teleport = StartCoroutine(ie_JumpingTeleportTiles(Tiles_Controller.instance.m_playerStartPosition.transform.localPosition, true, m_moveSpeed));
    }

    public void f_PlayJumpSfx()
    {
        if (Tiles_Controller.instance.m_lives != 0 && Tiles_Controller.instance.m_unsteppedTiles != 0)
        {
            SoundManager.instance.PlayJumpSfx();
        }
    }

    public void f_EnableJump()
    {
        m_enableJump = true;
    }

    public void f_DisableJump()
    {
        m_enableJump = false;
    }

    public void f_EndJumping()
    {
        if (Tiles_Controller.instance.m_lives != 0)
        {
            m_currentTile.f_SubstractSteps();

            foreach (GameObject item in Tiles_Controller.instance.m_enemies)
            {
                Enemy enemy = item.GetComponent<Enemy>();

                if (Tiles_Controller.instance.m_unsteppedTiles >= 0 && enemy.m_spawnedAtUnsteppedTiles != 0)
                {
                    if (Tiles_Controller.instance.m_unsteppedTiles <= enemy.m_spawnedAtUnsteppedTiles && enemy.m_isWaitingToSpawn && !enemy.gameObject.activeSelf)
                    {
                        Debug.Log("Spawn " + Tiles_Controller.instance.m_unsteppedTiles);
                        enemy.gameObject.SetActive(true);
                        enemy.Spawn();
                    }
                }
            }

            //teleport
            if (m_currentTile.m_secondBlackTiles != null && m_currentTile.m_tops == Tiles_Behaviour.e_tops.flashingBlackTops)
            {
                f_SetCurrentTile(m_currentTile.m_secondBlackTiles, true);
            }
        }
    }

    private IEnumerator ie_JumpingSmoothTiles(float p_time, Vector3 m_finalPos)
    {
        m_alreadyMove = true;
        m_animator.SetTrigger("OnJump");
        transform.DOMove(m_finalPos, p_time).SetEase(Ease.Linear);
        yield return new WaitForSeconds(p_time);
        m_animator.SetTrigger("OnLand");
        m_alreadyMove = false;
        yield return new WaitForSeconds(p_time / 2.25f);
        f_EndJumping();
    }

    private IEnumerator ie_JumpingTeleportTiles(Vector3 m_finalPos, bool isStart, float p_time)
    {
        m_alreadyMove = true;
        m_playerSprite.gameObject.SetActive(false);
        m_animator.SetTrigger("OnJump");
        transform.DOMove(m_finalPos, 0).SetEase(Ease.Linear);
        yield return new WaitForSeconds(0.1f);

        if (!isStart)
        {
            SoundManager.instance.PlayTeleportSfx();
            m_playerSprite.gameObject.SetActive(true);
            yield return new WaitForSeconds(0.15f);
            m_animator.SetTrigger("OnLand");
            m_alreadyMove = false;
            yield return new WaitForSeconds(p_time + 0.15f);
            f_EndJumping();
        }
        else
        {
            m_playerSprite.gameObject.SetActive(true);
            yield return new WaitForSeconds(0.15f);
            m_animator.SetTrigger("OnLand");
            m_alreadyMove = false;
        }
    }

    //private IEnumerator ie_SmoothLerp(float p_time, Vector3 m_startingPos, Vector3 m_finalPos)
    //{
    //    float elapsedTime = 0;

    //    while (elapsedTime < p_time)
    //    {
    //        transform.position = Vector3.Lerp(m_startingPos, m_finalPos, (elapsedTime / p_time));
    //        elapsedTime += Time.deltaTime;
    //        yield return null;
    //    }
    //}


    private void f_RandomFlip()
    {
        int a = Random.Range(0, 5);

        if (a == 1)
        {
            m_playerSprite.flipX = !m_playerSprite.flipX;
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Enemy")
        {
            if (Tiles_Controller.instance.m_lives != 0)
            {
                Debug.Log("KENA MUSUHHHH");
                SoundManager.instance.PlayEnemyHitSfx();
                Tiles_Controller.instance.m_lives--;
                Tiles_Controller.instance.f_CheckAndSetLive();
            }
        }
    }

}
