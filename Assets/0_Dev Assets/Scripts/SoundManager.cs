using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager instance;

    public AudioSource BGMAudio;
    public AudioSource SFXAudio;

    [SerializeField] private AudioClip[] BGMSound;
    [SerializeField] private AudioClip ButtonSfx;
    [SerializeField] private AudioClip[] JumpSfx;
    [SerializeField] private AudioClip OnJumpSfx;
    [SerializeField] private AudioClip AddScoreSfx;
    [SerializeField] private AudioClip TeleportSfx;
    [SerializeField] private AudioClip EnemyHitSfx;
    [SerializeField] private AudioClip EnemyPopSfx;
    [SerializeField] private AudioClip WinSfx;
    [SerializeField] private AudioClip LoseSfx;

    private int jumpIndex = 0;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }

        DontDestroyOnLoad(gameObject);
    }

    public void PlayBGM(int level) {
        if (level == 0)
        {
            BGMAudio.clip = BGMSound[0];
            BGMAudio.Play();
        }
        else if (level > 0 && level < 11) 
        {
            BGMAudio.clip = BGMSound[1];
            BGMAudio.Play();
        }
        else if (level > 10 && level < 21)
        {
            BGMAudio.clip = BGMSound[2];
            BGMAudio.Play();
        }
        else if (level > 20 && level < 31)
        {
            BGMAudio.clip = BGMSound[3];
            BGMAudio.Play();
        }
    }

    public void PlayButtonSfx()
    {
        SFXAudio.PlayOneShot(ButtonSfx);
    }

    public void PlayJumpSfx() {

        SFXAudio.PlayOneShot(JumpSfx[jumpIndex]);

        if (jumpIndex < JumpSfx.Length - 1)
        {
            jumpIndex++;
        }
        else {
            jumpIndex = 0;
        }
    }

    public void PlayTeleportSfx()
    {
        SFXAudio.PlayOneShot(TeleportSfx);
    }

    public void PlayEnemyHitSfx()
    {
        SFXAudio.PlayOneShot(EnemyHitSfx);
    }

    public void PlayEnemyPopSfx()
    {
        SFXAudio.PlayOneShot(EnemyPopSfx);
    }

    public void PlayOnJumpSfx() {
        SFXAudio.PlayOneShot(OnJumpSfx);
    }

    public void PlayAddScoreSfx()
    {
        SFXAudio.PlayOneShot(AddScoreSfx);
    }

    public void PlayWinSfx()
    {
        SFXAudio.PlayOneShot(WinSfx);
    }

    public void PlayLoseSfx()
    {
        SFXAudio.PlayOneShot(LoseSfx);
    }
}
