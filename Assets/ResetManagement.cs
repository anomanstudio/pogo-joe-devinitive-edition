using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetManagement : MonoBehaviour
{
    public GameObject resetPanel, settingPanel;
    // Start is called before the first frame update
    void Start()
    {
        closeResetPanel();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void openResetPanel(){
        resetPanel.SetActive(true);
        settingPanel.SetActive(false);
    }
    public void closeResetPanel(){
        resetPanel.SetActive(false);
        settingPanel.SetActive(true);
    }
}
