using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MainMenuLanguageSetting : MonoBehaviour
{
    public TMP_Text PlayText;
    public TMP_Text InstructionText;
    public TMP_Text SettingsText;

    [SerializeField] private string[] Play;
    [SerializeField] private string[] Instruction;
    [SerializeField] private string[] Settings;

    private void Awake()
    {
        
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        switch (GameDataManager.instance.Data.language)
        {
            case "English":
                Debug.Log("Setting Language to English");
                PlayText.text = Play[0];
                InstructionText.text = Instruction[0];
                SettingsText.text = Settings[0];

                break;

            case "Japanese":
                Debug.Log("Setting Language to Japanese");
                PlayText.text = Play[1];
                InstructionText.text = Instruction[1];
                SettingsText.text = Settings[1];

                break;

            case "Korean":
                Debug.Log("Setting Language to Korean");
                PlayText.text = Play[2];
                InstructionText.text = Instruction[2];
                SettingsText.text = Settings[2];

                break;

            case "ChineseS":
                Debug.Log("Setting Language to ChineseS");
                PlayText.text = Play[3];
                InstructionText.text = Instruction[3];
                SettingsText.text = Settings[3];

                break;

            case "ChineseT":
                Debug.Log("Setting Language to ChineseT");
                PlayText.text = Play[4];
                InstructionText.text = Instruction[4];
                SettingsText.text = Settings[4];

                break;

            case "Thai":
                Debug.Log("Setting Language to Thai");
                PlayText.text = Play[5];
                InstructionText.text = Instruction[5];
                SettingsText.text = Settings[5];

                break;

            case "Indonesia":
                Debug.Log("Setting Language to Indonesia");
                PlayText.text = Play[6];
                InstructionText.text = Instruction[6];
                SettingsText.text = Settings[6];

                break;

            case "Dutch":
                Debug.Log("Setting Language to Dutch");
                PlayText.text = Play[7];
                InstructionText.text = Instruction[7];
                SettingsText.text = Settings[7];

                break;

            case "French":
                Debug.Log("Setting Language to French");
                PlayText.text = Play[8];
                InstructionText.text = Instruction[8];
                SettingsText.text = Settings[8];

                break;

            case "German":
                Debug.Log("Setting Language to German");
                PlayText.text = Play[9];
                InstructionText.text = Instruction[9];
                SettingsText.text = Settings[9];

                break;

            case "Italian":
                Debug.Log("Setting Language to Italian");
                PlayText.text = Play[10];
                InstructionText.text = Instruction[10];
                SettingsText.text = Settings[10];

                break;

            case "Spanish":
                Debug.Log("Setting Language to Spanish");
                PlayText.text = Play[11];
                InstructionText.text = Instruction[11];
                SettingsText.text = Settings[11];

                break;

            case "Portuguese":
                Debug.Log("Setting Language to Portugese");
                PlayText.text = Play[12];
                InstructionText.text = Instruction[12];
                SettingsText.text = Settings[12];

                break;
        }
    }
}
