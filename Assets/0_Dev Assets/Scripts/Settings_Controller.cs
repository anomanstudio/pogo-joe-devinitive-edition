using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Settings_Controller : MonoBehaviour
{
    public TextMeshProUGUI m_txtPogoSpeed;
    public int m_pogoSpeed = 1;
    public Button m_btnIncreasePogoSpeed, m_btnDecreasePogoSpeed;

    public TextMeshProUGUI m_txtMonsterSpeed;
    public int m_monsterSpeed = 1;
    public Button m_btnIncreaseMonsterSpeed, m_btnDecreaseMonsterSpeed;

    public Sprite m_spriteOn, m_spriteOff;

    public Button m_btnMonsterDoubleJump;
    public bool m_monsterDoubleJumpOff;

    public Button m_btnBGM;
    public bool m_BgmOff;

    public Button m_btnSFX;
    public bool m_SFXOff;

    public SoundManager m_SoundManager;

    // Start is called before the first frame update
    void Start()
    {
        if (SoundManager.instance == null)
        {
            Instantiate(m_SoundManager);
        }

        if (Tiles_Controller.instance == null)
        {
            SoundManager.instance.PlayBGM(0);
        }
        else {
            SoundManager.instance.PlayBGM(Tiles_Controller.instance.m_levels);
        }

        LoadSettingsSaveData();
    }

    public void f_RefreshValue() {
        m_txtPogoSpeed.text = m_pogoSpeed.ToString();
        m_txtMonsterSpeed.text = m_monsterSpeed.ToString();

        f_CheckIncreaseDecreaseButton();
    }

    public void f_IncreasePogoSpeed()
    {
        SoundManager.instance.PlayButtonSfx();
        if (m_pogoSpeed <= 10) m_pogoSpeed++;
        m_txtPogoSpeed.text = m_pogoSpeed.ToString();

        f_CheckIncreaseDecreaseButton();
    }

    public void f_DecreasePogoSpeed()
    {
        SoundManager.instance.PlayButtonSfx();
        if (m_pogoSpeed > 0) m_pogoSpeed--;
        m_txtPogoSpeed.text = m_pogoSpeed.ToString();

        f_CheckIncreaseDecreaseButton();
    }

    public void f_IncreaseMonsterSpeed()
    {
        SoundManager.instance.PlayButtonSfx();
        if (m_monsterSpeed <= 10) m_monsterSpeed++;
        m_txtMonsterSpeed.text = m_monsterSpeed.ToString();

        f_CheckIncreaseDecreaseButton();
    }

    public void f_DecreaseMonsterSpeed()
    {
        SoundManager.instance.PlayButtonSfx();
        if (m_monsterSpeed > 0) m_monsterSpeed--;
        m_txtMonsterSpeed.text = m_monsterSpeed.ToString();

        f_CheckIncreaseDecreaseButton();
    }

    public void f_MonsterDoubleJUmp()
    {
        SoundManager.instance.PlayButtonSfx();
        if (!m_monsterDoubleJumpOff)
        {
            m_btnMonsterDoubleJump.image.sprite = m_spriteOn;
            m_monsterDoubleJumpOff = true;
        }
        else
        {
            m_btnMonsterDoubleJump.image.sprite = m_spriteOff;
            m_monsterDoubleJumpOff = false;
        }

        f_CheckIncreaseDecreaseButton();
    }

    public void f_BGM()
    {
        SoundManager.instance.PlayButtonSfx();

        if (!m_BgmOff)
        {
            m_btnBGM.image.sprite = m_spriteOn;
            m_BgmOff = true;
        }
        else
        {
            m_btnBGM.image.sprite = m_spriteOff;
            m_BgmOff = false;
        }

        SoundManager.instance.BGMAudio.mute = m_BgmOff;

        f_CheckIncreaseDecreaseButton();

        GameDataManager.instance.Data.isBgmOn = !m_BgmOff;
        GameDataManager.instance.Save();
    }

    public void f_SFX()
    {
        if (!m_SFXOff)
        {
            m_btnSFX.image.sprite = m_spriteOn;
            m_SFXOff = true;      
        }
        else
        {
            m_btnSFX.image.sprite = m_spriteOff;
            m_SFXOff = false;
            SoundManager.instance.PlayButtonSfx();
        }

        SoundManager.instance.SFXAudio.mute = m_SFXOff;

        f_CheckIncreaseDecreaseButton();

        GameDataManager.instance.Data.isSfxOn = !m_SFXOff;
        GameDataManager.instance.Save();
    }

    public void f_CheckIncreaseDecreaseButton()
    {
        if (m_btnDecreasePogoSpeed != null)
            m_btnDecreasePogoSpeed.image.enabled = (m_pogoSpeed == 1) ? false : true;

        if (m_btnIncreasePogoSpeed != null)
            m_btnIncreasePogoSpeed.image.enabled = (m_pogoSpeed == 10) ? false : true;

        if (m_btnDecreaseMonsterSpeed != null)
            m_btnDecreaseMonsterSpeed.image.enabled = (m_monsterSpeed == 1) ? false : true;

        if (m_btnIncreaseMonsterSpeed != null)
            m_btnIncreaseMonsterSpeed.image.enabled = (m_monsterSpeed == 10) ? false : true;

        if (m_btnMonsterDoubleJump != null)
            m_btnMonsterDoubleJump.image.sprite = (m_monsterDoubleJumpOff == false) ? m_spriteOn : m_spriteOff;

        if (m_btnBGM != null)
            m_btnBGM.image.sprite = (m_BgmOff == false) ? m_spriteOn : m_spriteOff;

        if (m_btnSFX != null)
            m_btnSFX.image.sprite = (m_SFXOff == false) ? m_spriteOn : m_spriteOff;
    }

    private void LoadSettingsSaveData() {
        m_BgmOff = !GameDataManager.instance.Data.isBgmOn;
        m_SFXOff = !GameDataManager.instance.Data.isSfxOn;

        if (m_BgmOff)
        {
            SoundManager.instance.BGMAudio.mute = true;
            m_btnSFX.image.sprite = m_spriteOff;
        }
        else {
            m_btnSFX.image.sprite = m_spriteOn;
        }

        if (m_SFXOff)
        {
            SoundManager.instance.SFXAudio.mute = true;
            m_btnSFX.image.sprite = m_spriteOff;
        }
        else {
            m_btnSFX.image.sprite = m_spriteOn;
        }
    }
}
