using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class Engine_InitialLevelSetup : MonoBehaviour
{
#if UNITY_EDITOR
    [MenuItem("AnomanStudio/PogoJoe/InitLevel %&s")]

    static void CreateAPrefab()
    {
        DestroyImmediate(Camera.main.gameObject);

        //Player
        GameObject m_player = (GameObject)PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath<Object>("Assets/0_Dev Assets/Prefabs/Setup/Player.prefab"));
        m_player.name = "Player";

        m_player.transform.localPosition = Vector3.zero;
        m_player.transform.localEulerAngles = Vector3.zero;
        m_player.transform.localScale = Vector3.one;

        //Tile
        GameObject m_tiles = (GameObject)PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath<Object>("Assets/0_Dev Assets/Prefabs/Setup/Tiles.prefab"));
        m_tiles.name = "Tiles";

        m_tiles.transform.localPosition = Vector3.zero;
        m_tiles.transform.localEulerAngles = Vector3.zero;
        m_tiles.transform.localScale = Vector3.one;

        //Game Controller
        GameObject m_gameController = (GameObject)PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath<Object>("Assets/0_Dev Assets/Prefabs/Setup/GAME CONTROLLER.prefab"));
        m_gameController.name = "GAME CONTROLLER";

        m_gameController.transform.localPosition = Vector3.zero;
        m_gameController.transform.localEulerAngles = Vector3.zero;
        m_gameController.transform.localScale = Vector3.one;

        //Game Controller
        GameObject m_eventSystem = (GameObject)PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath<Object>("Assets/0_Dev Assets/Prefabs/Setup/EventSystem.prefab"));
        m_eventSystem.name = "EventSystem";

        m_eventSystem.transform.localPosition = Vector3.zero;
        m_eventSystem.transform.localEulerAngles = Vector3.zero;
        m_eventSystem.transform.localScale = Vector3.one;

        //Main Camera
        GameObject m_mainCamera = (GameObject)PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath<Object>("Assets/0_Dev Assets/Prefabs/Setup/Main Camera.prefab"));
        m_mainCamera.name = "Main Camera";

        m_mainCamera.transform.localPosition = new Vector3(0, 0, -10);
        m_mainCamera.transform.localEulerAngles = Vector3.zero;
        m_mainCamera.transform.localScale = Vector3.one;

        //Background Up
        GameObject m_backgroundUp = (GameObject)PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath<Object>("Assets/0_Dev Assets/Prefabs/Setup/Background Up.prefab"));
        m_backgroundUp.name = "Background Up";

        m_backgroundUp.transform.localPosition = Vector3.zero;
        m_backgroundUp.transform.localEulerAngles = Vector3.zero;
        m_backgroundUp.transform.localScale = Vector3.one;

        //Background Down
        GameObject m_backgroundDown = (GameObject)PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath<Object>("Assets/0_Dev Assets/Prefabs/Setup/Background Down.prefab"));
        m_backgroundDown.name = "Background Down";

        m_backgroundDown.transform.localPosition = Vector3.zero;
        m_backgroundDown.transform.localEulerAngles = Vector3.zero;
        m_backgroundDown.transform.localScale = Vector3.one;

        m_player.GetComponent<Player_Controller>().m_currentTile = m_tiles.GetComponent<Tiles_Behaviour>();
        //m_tiles.GetComponent<Tiles_Behaviour>().m_tilesController = m_gameController.GetComponent<Tiles_Controller>();

        m_gameController.GetComponent<Tiles_Controller>().m_BGUp = m_backgroundUp.GetComponent<SpriteRenderer>();
        m_gameController.GetComponent<Tiles_Controller>().m_BGDown = m_backgroundDown.GetComponent<SpriteRenderer>();
        m_gameController.GetComponent<Tiles_Controller>().m_gameplayGUI = m_gameController.GetComponent<GamePlay_GUI>();

        //Enemy
        GameObject m_enemy = (GameObject)PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath<Object>("Assets/0_Dev Assets/Prefabs/Setup/Enemy.prefab"));
        m_enemy.name = "Enemy";

        m_enemy.transform.localPosition = Vector3.zero;
        m_enemy.transform.localEulerAngles = Vector3.zero;
        m_enemy.transform.localScale = Vector3.one;

        m_enemy.GetComponent<Enemy>().m_currentTile = m_tiles.GetComponent<Tiles_Behaviour>();
    }
#endif
}
