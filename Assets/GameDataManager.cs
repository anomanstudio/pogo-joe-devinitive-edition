using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class GameDataManager : MonoBehaviour
{
    public static GameDataManager instance;

    [SerializeField] private string file = "gameData.json";

    public GameData Data;

    private void Awake()
    {
        if (instance != null)
            Destroy(gameObject);
        else
            instance = this;

        Load();

        DontDestroyOnLoad(gameObject);
    }

    public void Load()
    {
        string json = ReadFromFIle(file);
        JsonUtility.FromJsonOverwrite(json, Data);
    }

    public void Save()
    {
        string json = JsonUtility.ToJson(Data);
        WriteToFile(file, json);
    }

    public void ResetGame() {
        Debug.Log("Save Data Reset !!!"); 

        Data = new GameData();
        Data.isBgmOn = true;
        Data.isSfxOn = true;
        Data.language = "English";

        string json = JsonUtility.ToJson(Data);
        WriteToFile(file, json);
    }

    private void WriteToFile(string fileName, string json)
    {
        string path = GetFilePath(fileName);
        FileStream fileStream = new FileStream(path, FileMode.Create);

        using (StreamWriter writer = new StreamWriter(fileStream))
        {
            Debug.Log("Save Data !!!");
            writer.Write(json);
        }
    }

    private string ReadFromFIle(string fileName)
    {
        string path = GetFilePath(fileName);
        if (File.Exists(path))
        {
            using (StreamReader reader = new StreamReader(path))
            {
                string json = reader.ReadToEnd();
                return json;
            }
        }
        else
        {
            Debug.Log("Save File not found, Create New Data");

            Data = new GameData();
            Data.isBgmOn = true;
            Data.isSfxOn = true;
            Data.language = "English";

            string json = JsonUtility.ToJson(Data);
            WriteToFile(file, json);
        }

        return "Success";
    }

    private string GetFilePath(string fileName)
    {
        return Application.persistentDataPath + "/" + fileName;
    }

    [System.Serializable]
    public class GameData {
        public bool isBgmOn; 
        public bool isSfxOn;
        public int score;
        public int highscore;

        public string language;
    }
}
