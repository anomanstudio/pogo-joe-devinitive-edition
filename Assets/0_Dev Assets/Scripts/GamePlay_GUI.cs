using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GamePlay_GUI : MonoBehaviour
{
    public CanvasGroup m_canvasGroupPause, m_canvasGroupSettings, m_canvasGroupQuit, m_canvasGroupGameOver, m_canvasGroupWin;
    public TextMeshProUGUI m_txtGameOverScore, m_txtWinScore;

    Tiles_Controller m_TilesController;

    // Start is called before the first frame update
    void Start()
    {
        m_TilesController = GetComponent<Tiles_Controller>();
    }

    // Loading Display
    public CanvasGroup m_canvasGroupLoadingScreen;
    public Image LoadingBarFill;

    public void f_ChangeScene(string p_scene)
    {
        StartCoroutine(ie_LoadSceneAsync(p_scene));
    }
    IEnumerator ie_LoadSceneAsync(string p_scene)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(p_scene);

        f_ShowCanvasGroup(m_canvasGroupLoadingScreen);

        while (!operation.isDone)
        {
            float progressValue = Mathf.Clamp01(operation.progress / 0.9f);
            LoadingBarFill.fillAmount = progressValue;
            yield return null;
        }
    }

    public void f_ShowCanvasGroup(CanvasGroup p_canvas) {
        p_canvas.alpha = 1;
        p_canvas.interactable = true;
        p_canvas.blocksRaycasts = true;
    }

    public void f_HideCanvasGroup(CanvasGroup p_canvas) {
        p_canvas.alpha = 0;
        p_canvas.interactable = false;
        p_canvas.blocksRaycasts = false;
    }

    public void f_ClickBtnPause() {
        SoundManager.instance.PlayButtonSfx();
        f_ShowCanvasGroup(m_canvasGroupPause);
        Time.timeScale = 0;
    }

    public void f_ClickBtnResume() {
        SoundManager.instance.PlayButtonSfx();
        f_HideCanvasGroup(m_canvasGroupPause);
        Time.timeScale = 1;
    }

    //SETTINGS
    public void f_ClickBtnSettings() {
        SoundManager.instance.PlayButtonSfx();
        GetComponent<Settings_Controller>().f_RefreshValue();
        f_HideCanvasGroup(m_canvasGroupPause);
        f_ShowCanvasGroup(m_canvasGroupSettings);
    }

    public void f_ClickBtnSettingsDone() {
        SoundManager.instance.PlayButtonSfx();
        f_HideCanvasGroup(m_canvasGroupSettings);
        f_ShowCanvasGroup(m_canvasGroupPause);
    }


    //QUIT
    public void f_ClickBtnQuit() {
        SoundManager.instance.PlayButtonSfx();
        f_HideCanvasGroup(m_canvasGroupPause);
        f_ShowCanvasGroup(m_canvasGroupQuit);
    }

    public void f_ClickBtnQuitYes() {
        Time.timeScale = 1;
        f_ChangeScene("Main Menu");
    }

    public void f_ClickBtnQuitNo() {
        SoundManager.instance.PlayButtonSfx();
        f_HideCanvasGroup(m_canvasGroupQuit);
        f_ShowCanvasGroup(m_canvasGroupPause);
    }

    //GAME OVER
    public void f_ShowGameOverGUI() {
        SoundManager.instance.PlayLoseSfx();
        m_txtGameOverScore.text = m_TilesController.m_scores.ToString();
        f_ShowCanvasGroup(m_canvasGroupGameOver);

        if (m_TilesController.m_scores > GameDataManager.instance.Data.highscore)
        {
            GameDataManager.instance.Data.highscore = m_TilesController.m_scores;

        }

        GameDataManager.instance.Data.score = 0;
        GameDataManager.instance.Save();
    }

    public void f_ClickBtnPlayAgain()
    {
        //restart level
        SoundManager.instance.PlayButtonSfx();
        SceneManager.LoadScene("Level 1");
    }

    public void f_ClickBtnMainMenu() {
        //to menu scene
        SoundManager.instance.PlayButtonSfx();
        SceneManager.LoadScene(0);
        //bgm switched to main menu bgm
    }

    //WIN
    public void f_ShowWinGUI() {
        SoundManager.instance.PlayWinSfx();
        m_txtWinScore.text = m_TilesController.m_scores.ToString();
        f_ShowCanvasGroup(m_canvasGroupWin);
        
        if (m_TilesController.m_scores > GameDataManager.instance.Data.highscore)
        {
            GameDataManager.instance.Data.highscore = m_TilesController.m_scores;
         
        }

        GameDataManager.instance.Data.score = m_TilesController.m_scores;
        GameDataManager.instance.Save();
    }

    public void f_ClickBtnNextLevel()
    {
        //next level
        if (m_TilesController.m_levels < 30)
        {
            Debug.Log("Level " + (m_TilesController.m_levels + 1));
            SceneManager.LoadScene("Level " + (m_TilesController.m_levels + 1));
        }
        else {
            SceneManager.LoadScene(0);
        }
       
    }

    public void f_ClickBtnHighScore()
    {
        //show high score
    }
}
