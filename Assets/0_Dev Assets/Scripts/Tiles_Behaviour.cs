using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Tiles_Behaviour : MonoBehaviour
{
    public List<Color> m_colors = new List<Color>();

    public int m_spriteSteps = 1;
    public SpriteRenderer m_topsSpriteRenderer;

    //tiles node
    public Tiles_Behaviour m_tilesUp, m_tilesDown, m_tilesLeft, m_tilesRight;

    public float m_rayLength = 10;

    public enum e_tops
    {
        whiteTops,
        cyanTops,
        purpleTops,
        flashingGreenTops,
        flashingBlackTops
    }

    public e_tops m_tops;

    public Animator m_tileAnimator;

    public Tiles_Behaviour m_secondBlackTiles; //must be filled if you choose black flashing tiles

    // Start is called before the first frame update
    void Start()
    {
        Tiles_Controller.instance.f_AddTiles(this);
        f_SetSpriteColorBySteps();
        f_CheckNodes();
        f_CheckTops();
    }

    private void Update()
    {
        f_DrawCheckNode(Vector2.up);
        f_DrawCheckNode(-Vector2.up);
        f_DrawCheckNode(Vector2.right);
        f_DrawCheckNode(-Vector2.right);
    }

    public void f_CheckTops()
    {
        if (m_tops == e_tops.whiteTops) f_WhiteTops();
        else if (m_tops == e_tops.cyanTops) f_CyanTops();
        else if (m_tops == e_tops.purpleTops) f_PurpleTops();
        else if (m_tops == e_tops.flashingGreenTops) f_FlashingGreenTops();
        else if (m_tops == e_tops.flashingBlackTops) f_FlashingBlackTops();
    }

    public void f_WhiteTops()
    { //default 
        m_spriteSteps = 1;
        m_topsSpriteRenderer.color = m_colors[1];
        m_tileAnimator.enabled = false;

        gameObject.name = "White Barrel";
    }

    public void f_CyanTops() //two times
    {
        m_spriteSteps = 2;
        m_topsSpriteRenderer.color = m_colors[2];
        m_tileAnimator.enabled = false;

        gameObject.name = "Cyan Barrel";
    }

    public void f_PurpleTops() //three times
    {
        m_spriteSteps = 3;
        m_topsSpriteRenderer.color = m_colors[3];
        m_tileAnimator.enabled = false;

        gameObject.name = "Purple Barrel";
    }

    public void f_FlashingGreenTops() //kill enemies
    {
        m_spriteSteps = 1;
        m_tileAnimator.SetInteger("flashingID", 2);

        gameObject.name = "Flash Green Barrel";
    }

    public void f_FlashingBlackTops() //teleport
    {
        m_spriteSteps = 0;
        if (m_secondBlackTiles != null)
        {
            m_tileAnimator.SetInteger("flashingID", 1);
        }
        else Debug.LogError("PLEASE DEFINE THE SECOND BLACK TILE!!!");

        gameObject.name = "Flash Black Barrel";
    }

    public void f_SubstractSteps()
    {
        if (m_tops != e_tops.flashingBlackTops)
        {
            if (m_spriteSteps > 0)
            { 
                m_spriteSteps--;
                Tiles_Controller.instance.f_SubstractUnSteppedTiles();

                if (m_tops == e_tops.flashingGreenTops)
                {
                    //destroy enemies
                    SoundManager.instance.PlayEnemyPopSfx();
                    Tiles_Controller.instance.f_DestroyEnemies();
                    m_tileAnimator.enabled = false;
                }
                else {
                    SoundManager.instance.PlayAddScoreSfx();
                }
            }

            f_SetSpriteColorBySteps();
        }
    }

    public void f_AddSteps()
    {
        if (m_tops == e_tops.whiteTops)
        {
            if (m_spriteSteps < 1)
            {
                m_spriteSteps++;
                Tiles_Controller.instance.f_AddUnSteppedTiles();
            }

            f_SetSpriteColorBySteps();
        }
        else if (m_tops == e_tops.cyanTops)
        {
            if (m_spriteSteps < 2)
            {
                m_spriteSteps++;
                Tiles_Controller.instance.f_AddUnSteppedTiles();
            }

            f_SetSpriteColorBySteps();
        }
        else if (m_tops == e_tops.purpleTops)
        {
            if (m_spriteSteps < 3)
            {
                m_spriteSteps++;
                Tiles_Controller.instance.f_AddUnSteppedTiles();
            }

            f_SetSpriteColorBySteps();
        }
    }

    public void f_SetSpriteColorBySteps()
    {
        m_topsSpriteRenderer.color = m_colors[m_spriteSteps];
    }

    public void f_CheckNodes()
    {
        f_CheckNode(Vector2.up);
        f_CheckNode(-Vector2.up);
        f_CheckNode(Vector2.right);
        f_CheckNode(-Vector2.right);
    }

    void f_CheckNode(Vector2 m_direction)
    {
        //Cast a ray in the direction specified in the inspector.
        RaycastHit2D hit = Physics2D.Raycast(transform.position, transform.TransformDirection(m_direction), m_rayLength, LayerMask.GetMask("Tiles"));

        //If something was hit.
        if (hit)
        {
            if (m_direction == Vector2.up) m_tilesUp = hit.collider.GetComponent<Tiles_Behaviour>();
            else if (m_direction == -Vector2.up) m_tilesDown = hit.collider.GetComponent<Tiles_Behaviour>();
            else if (m_direction == Vector2.right) m_tilesRight = hit.collider.GetComponent<Tiles_Behaviour>();
            else if (m_direction == -Vector2.right) m_tilesLeft = hit.collider.GetComponent<Tiles_Behaviour>();
        }
    }

    void f_DrawCheckNode(Vector2 m_direction)
    {
        Debug.DrawRay(transform.position, transform.TransformDirection(m_direction) * m_rayLength, Color.green);
    }


}
