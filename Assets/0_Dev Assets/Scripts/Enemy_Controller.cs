using UnityEngine;

public class Enemy_Controller : MonoBehaviour
{
    public enum e_enemies
    {
        ghost1,
        ghost2,
        ghostBlue,
        ghostOrange,
        ghostPurple
    }

    public e_enemies m_enemiesChoice;

    public GameObject[] m_enemies;

    public SpriteRenderer m_currentEnemySprite;

    void Start()
    {
        f_DeactiveAllGhostObjects();

        if (m_enemiesChoice == e_enemies.ghost1)
        {
            f_SetGhost(0);
            gameObject.name = "Enemy Ghost1";
        }
        else if (m_enemiesChoice == e_enemies.ghost2)
        {
            f_SetGhost(1);
            gameObject.name = "Enemy Ghost2";
        }
        else if (m_enemiesChoice == e_enemies.ghostBlue)
        {
            f_SetGhost(2);
            gameObject.name = "Enemy GhostBlue";
        }
        else if (m_enemiesChoice == e_enemies.ghostOrange)
        {
            f_SetGhost(3);
            gameObject.name = "Enemy GhostOrange";
        }
        else if (m_enemiesChoice == e_enemies.ghostPurple)
        {
            f_SetGhost(4);
            gameObject.name = "Enemy GhostPurple";
        }
    }

    void f_DeactiveAllGhostObjects()
    {
        for (int i = 0; i < m_enemies.Length; i++)
        {
            m_enemies[i].SetActive(false);
        }
    }

    void f_SetGhost(int p_ghostId)
    {
        m_enemies[p_ghostId].SetActive(true);
        m_currentEnemySprite = m_enemies[p_ghostId].GetComponent<SpriteRenderer>();
    }

    public void f_CheckingEnemySkill(Tiles_Behaviour tile)
    {
        if (m_enemiesChoice != e_enemies.ghost1 && m_enemiesChoice != e_enemies.ghost2)
        {
            f_EnemySubstactSkill(tile);
        }
    }

    void f_EnemySubstactSkill(Tiles_Behaviour tile)
    {
        if (tile.m_tops != Tiles_Behaviour.e_tops.flashingBlackTops && tile.m_tops != Tiles_Behaviour.e_tops.flashingGreenTops)
        {
            tile.f_AddSteps();
        }
    }
}
