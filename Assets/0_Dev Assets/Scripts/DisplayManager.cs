using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class DisplayManager : MonoBehaviour
{
    // Loading Display
    public GameObject LoadingScreen;
    public Image LoadingBarFill;
    public TextMeshProUGUI highScoretxt;

    public void LoadScene(string p_scene){
        GameDataManager.instance.Data.score = 0;
        GameDataManager.instance.Save();
        StartCoroutine(LoadSceneAsync(p_scene));
    }
    IEnumerator LoadSceneAsync(string p_scene)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(p_scene);

        LoadingScreen.SetActive(true);

        while(!operation.isDone){
            float progressValue = Mathf.Clamp01(operation.progress / 0.9f);
            LoadingBarFill.fillAmount = progressValue;
            yield return null;
        }
    }
    // <---------------------------------- end of loading display ---------------------------------->//

    // Start Scene
    void Start(){
        HideDisplayInstruction();
        HideDisplaySettings();
        HideDisplayExit();

        LoadHighscore();
    }

    public void LoadHighscore() {
        highScoretxt.text = GameDataManager.instance.Data.highscore.ToString();
    }

    // Instruction Display
    public GameObject InstructionDisplay;

    public void ShowDisplayInstruction(){
        SoundManager.instance.PlayButtonSfx();
        InstructionDisplay.SetActive(true);
    }

    public void HideDisplayInstruction(){
        SoundManager.instance.PlayButtonSfx();
        InstructionDisplay.SetActive(false);
    }

    // <---------------------------------- end of Instruction display ---------------------------------->//

    // Settings Display
    public GameObject SettingsDisplay;
    public Settings_Controller m_settingsController;

    public void ShowDisplaySettings(){
        SoundManager.instance.PlayButtonSfx();
        m_settingsController.f_RefreshValue();
        SettingsDisplay.SetActive(true);
    }

    public void HideDisplaySettings(){
        SoundManager.instance.PlayButtonSfx();
        SettingsDisplay.SetActive(false);
    }

    // <---------------------------------- end of Settings display ---------------------------------->//

    // Exit Display
    public GameObject ExitDisplay;


    public void ShowDisplayExit(){
        SoundManager.instance.PlayButtonSfx();
        ExitDisplay.SetActive(true);
    }

    public void HideDisplayExit(){
        SoundManager.instance.PlayButtonSfx();
        ExitDisplay.SetActive(false);
    }

    // <---------------------------------- end of Exit display ---------------------------------->//  
   

    public void quit()
    {
        SoundManager.instance.PlayButtonSfx();
        Application.Quit();
    }

    public void ResetGameProgress() {
        SoundManager.instance.PlayButtonSfx();
        GameDataManager.instance.ResetGame();
        LoadHighscore();
    }
}
