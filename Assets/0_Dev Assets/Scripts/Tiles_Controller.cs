using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
using System;

public class Tiles_Controller : MonoBehaviour
{
    public static Tiles_Controller instance;

    public List<Tiles_Behaviour> m_tiles = new List<Tiles_Behaviour>();

    public int m_unsteppedTiles;

    public int m_lives = 3;
    public Image m_ImgLives;

    public int m_levels = 0;
    public TextMeshProUGUI m_txtLevels;

    public int m_scores = 0;
    public TextMeshProUGUI m_txtScores;

    public SpriteRenderer m_BGUp, m_BGDown;
    public Sprite[] m_BGUps;
    public Sprite[] m_BGDowns;

    public GamePlay_GUI m_gameplayGUI;

    public List<GameObject> m_enemies = new List<GameObject>();

    public Tiles_Behaviour m_playerStartPosition;
    public Player_Controller m_player;

    public bool isPlay;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }

        m_tiles.Clear();
        m_unsteppedTiles = 0;

        StartCoroutine(delayStart());
    }

    private void Start()
    {
        m_gameplayGUI = gameObject.GetComponent<GamePlay_GUI>();
        f_SetScore();
        f_SetLevel();

        m_scores = GameDataManager.instance.Data.score;
        f_SetScore();
    }

    private void Update()
    {
        int a = 0;
        foreach (Tiles_Behaviour item in m_tiles)
        {
            if (item.m_tops != Tiles_Behaviour.e_tops.flashingBlackTops)
            {
                //if (item.m_tops == Tiles_Behaviour.e_tops.cyanTops)
                //{
                //    a += 2;
                //}
                //else if (item.m_tops == Tiles_Behaviour.e_tops.purpleTops)
                //{
                //    a += 3;
                //}
                //else
                //{
                //    a++;
                //}
                a += item.m_spriteSteps;
            }
        }
        m_unsteppedTiles = a;
    }

    public void f_AddTiles(Tiles_Behaviour p_tile)
    {
        if (!m_tiles.Contains(p_tile))
        {
            m_tiles.Add(p_tile);
        }
    }

    public void f_SubstractUnSteppedTiles()
    {
        m_unsteppedTiles--;

        //Add Score
        f_AddScore(10);
        if (m_unsteppedTiles == 0)
        {
            // Win
            m_gameplayGUI.f_ShowWinGUI();
        }
    }

    public void f_AddUnSteppedTiles()
    {
        m_unsteppedTiles++;
        //Min Score
        f_MinScore(10);
    }

    public void f_CheckAndSetLive()
    {
        if (m_lives == 0)
        {
            //Game Over
            m_gameplayGUI.f_ShowGameOverGUI();
        }
        else
        {
            m_ImgLives.fillAmount = (float)m_lives * (10f / 3f) / 10f;

            //Player Respawn
            m_player.m_currentTile = m_playerStartPosition;
            m_player.f_Respawn();
        }
    }

    public void f_SetLevel()
    {
        //m_levels = 1;

        string p_level = m_levels.ToString();
        if (p_level.Length == 2) m_txtLevels.text = "Level " + p_level;
        else if (p_level.Length == 1) m_txtLevels.text = "Level 0" + p_level;
        else m_txtLevels.text = "Level 00";

        if (m_levels >= 1 && m_levels <= 10)
        {
            m_BGUp.sprite = m_BGUps[0];
            m_BGDown.sprite = m_BGDowns[0];
        }
        else if (m_levels >= 11 && m_levels <= 20)
        {
            m_BGUp.sprite = m_BGUps[1];
            m_BGDown.sprite = m_BGDowns[1];
        }
        else if (m_levels >= 20 && m_levels <= 30)
        {
            m_BGUp.sprite = m_BGUps[2];
            m_BGDown.sprite = m_BGDowns[2];
        }
        else if (m_levels >= 30)
        {
            m_BGUp.sprite = m_BGUps[3];
            m_BGDown.sprite = m_BGDowns[3];
        }
    }

    public void f_SetScore()
    {
        string p_score = m_scores.ToString();

        if (p_score.Length > 8)
        {
            m_txtScores.text = "SCORE IS TOO HIGH";
        }
        else
        {
            string p_repeatZero = new string('0', 8 - (p_score.Length));
            m_txtScores.text = p_repeatZero + "" + p_score;
        }

        //SAVE SCORE
    }

    public void f_DestroyEnemies()
    {
        if (m_enemies.Count > 0)
        {
            for (int i = 0; i < m_enemies.Count; i++)
            {
                f_AddScore(20);
                if (m_enemies[i].activeSelf)
                {
                    //Destroy(m_enemies[i]);
                    //m_enemies.Remove(m_enemies[i]);
                    m_enemies[i].GetComponent<Enemy>().Hide();
                }
            }
        }
    }

    public void f_AddScore(int p_add)
    {
        if(isPlay)
        {
            m_scores += p_add;
            f_SetScore();
        }
    }

    public void f_MinScore(int p_add)
    {
        m_scores -= p_add;
        f_SetScore();
    }

    IEnumerator delayStart()
    {
        yield return new WaitForSeconds(0.5f);
        isPlay = true;
    }
}
