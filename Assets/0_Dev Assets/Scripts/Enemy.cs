using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public Tiles_Behaviour m_currentTile;
    public float m_delayMove = 1;
    public float m_moveSpeed = 1;
    public bool m_isWaitingToSpawn;
    public int m_spawnedAtUnsteppedTiles = 10;

    public Enemy_Controller m_enemyController;

    public float m_delayTime = 2f;
    [SerializeField] private bool m_isHiding;

    // Start is called before the first frame update
    void Start()
    {
        if (m_isWaitingToSpawn)
        {
            gameObject.SetActive(false);
        }
        else {
            //m_currentTile.f_SubstractSteps();

            StartCoroutine(ie_SmoothLerp(m_moveSpeed, transform.localPosition, m_currentTile.transform.localPosition));
        }
    }

    public void Spawn()
    {
        StartCoroutine(ie_SmoothLerp(m_moveSpeed, transform.localPosition, m_currentTile.transform.localPosition));
    }


    public void f_Move()
    {
        if(!m_isHiding)
        {
            int movement = Random.Range(1, 5);

            if (movement == 1)
        {
            if (m_currentTile.m_tilesDown != null) f_SetCurrentTile(m_currentTile.m_tilesDown);
            else f_Move();
        }

        else if (movement == 2)
        {
            if (m_currentTile.m_tilesUp != null) f_SetCurrentTile(m_currentTile.m_tilesUp);
            else f_Move();
        }


        else if (movement == 3)
        {
            if (m_currentTile.m_tilesLeft != null)
            {
                f_SetCurrentTile(m_currentTile.m_tilesLeft);
                m_enemyController.m_currentEnemySprite.flipX = false;
            }
            else f_Move();
        }


        else if (movement == 4)
        {
            if (m_currentTile.m_tilesRight != null)
            {
                f_SetCurrentTile(m_currentTile.m_tilesRight);
                m_enemyController.m_currentEnemySprite.flipX = true;
            }
            else f_Move();
        }
        }
    }

    public void f_SetCurrentTile(Tiles_Behaviour p_tile)
    {
        m_currentTile = p_tile;

        StartCoroutine(ie_SmoothLerp(m_moveSpeed, transform.localPosition, p_tile.transform.localPosition));
    }

    private IEnumerator ie_SmoothLerp(float p_time, Vector3 m_startingPos, Vector3 m_finalPos)
    {
        if (!m_isHiding)
        {
        transform.DOMove(m_finalPos, p_time).SetEase(Ease.Linear);
        yield return new WaitForSeconds(p_time);
        m_enemyController.f_CheckingEnemySkill(m_currentTile);
        }

        yield return new WaitForSeconds(m_delayMove);
        f_Move();
    }

    public void Hide()
    {
        StartCoroutine(hideEnemy());
    }

    IEnumerator hideEnemy()
    {
        m_enemyController.m_currentEnemySprite.gameObject.SetActive(false);
        m_isHiding = true;
        yield return new WaitForSeconds(m_delayTime);
        m_enemyController.m_currentEnemySprite.gameObject.SetActive(true);
        m_isHiding = false;
        f_Move();
    }

}
