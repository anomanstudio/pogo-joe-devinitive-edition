using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_1 : MonoBehaviour
{
    public Tiles_Behaviour m_currentTile;
    public float m_moveSpeed = 1f;
    //public float m_range = 3;

    //float startingY;
    //int dir = 1;

    public Vector3 direction = Vector3.zero;
    bool running = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        


        if (!running)
        {
            StartCoroutine(changeDirection());
        }
        transform.position += direction * m_moveSpeed;
    }
    
    

    IEnumerator changeDirection()
    {
        running = true;
        yield return new WaitForSeconds(2);
        /*for (int i = 0; i < 2; i++)
        {
            if (m_currentTile.m_tilesDown != null) Random.Range(-1, 2);
            if(m_currentTile.m_tilesUp != null) Random.Range(-1, 2);
            if (m_currentTile.m_tilesLeft != null) Random.Range(-1, 2);
            if (m_currentTile.m_tilesRight != null) Random.Range(-1, 2);
        }*/
        direction.x = Random.Range(-1, 2);
        direction.y = Random.Range(-1, 2);
        running = false;
    }
}
